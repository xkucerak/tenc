#include "tensor.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

////////////////////////////////////////////
/// VECTOR 
////////////////////////////////////////////

struct Vector_S createVector(size_t size, size_t* values) {

    size_t *valuesCopy = malloc(sizeof(size_t) * size);
    
    for(size_t i = 0u; i < size; i++) {
        valuesCopy[i] = values[i];
    }

    return (struct Vector_S) {valuesCopy, size} ;
} 

struct Vector_S copyVector(struct Vector_S vector) {
    return createVector(vector.size, vector.values);
}

void destroyVector(struct Vector_S *vector) {
    free(vector->values);
} 


////// FOLDING 


size_t foldStepSum(size_t acc, size_t val) {
    return acc + val;
}

size_t foldStepMul(size_t acc, size_t val) {
    return acc * val;
}

size_t vectorFold(struct Vector_S vector, size_t (*step)(size_t, size_t), size_t acc) {
    for(size_t i = 0u; i < vector.size; i++) {
        acc = (*step)(acc, vector.values[i]);
    }
    return acc;
}

size_t vectorSum(struct Vector_S vector) {
    return vectorFold(vector, foldStepSum, 0);
}

size_t vectorProduct(struct Vector_S vector) {
    return vectorFold(vector, foldStepMul, 1);
}

////// MISC 

struct Vector_S getLevelSizes(struct Vector_S shape) {
    size_t *values = malloc(sizeof(size_t) * shape.size);
    size_t currentLevel = 1;
    for(size_t i = 0u; i < shape.size; i++) {
        values[shape.size - 1 - i] = currentLevel;
        currentLevel *= shape.values[shape.size - 1 - i];
    }

    return (struct Vector_S) {values, shape.size};
}

void printVectorS(struct Vector_S vector) {
    putchar('[');
    for(size_t i = 0u; i < vector.size; i++) {
        printf("%zu", vector.values[i]);
        if(i < vector.size - 1) {
            printf(", ");
        }
    }
    putchar(']');
}

////////////////////////////////////////////
/// TENSOR
////////////////////////////////////////////

////// CONTAINER 

bool decrementReferenceCounter(struct ValueContainer *container) {
    container->references--;
    
    if(container->references > 0) {
        return 0;
    }

    free(container->values);
    free(container);
    return 1;
}

////// TENSOR 

struct Tensor cTensorByFun(struct Vector_S shape, value_t (*generator)(size_t)) {
    
    size_t size = vectorProduct(shape);
    
    value_t* values = (value_t *) malloc(sizeof(value_t) * size);
    for(size_t i = 0u; i < size; i++) {
        values[i] = generator(i);
    }

    struct ValueContainer *container = malloc(sizeof(struct ValueContainer)); 
    *container = (struct ValueContainer) {values, size, 1};

    struct Tensor res = (struct Tensor) { 
        container, 
        shape, 
        getLevelSizes(shape), 
        basicGet, 
        basicGetC 
    };
    return res;
}

struct Tensor cTensorByFunLike(struct Tensor *tensor, value_t (*generator)(size_t)) {
    
    return cTensorByFun(copyVector(tensor->shape), generator);

}

void destroyTensor(struct Tensor tensor) {
    decrementReferenceCounter(tensor.values);
    destroyVector(&tensor.shape);
    destroyVector(&tensor.levelSizes);
}


////// GETTING 


value_t basicGet(struct Tensor *tensor, size_t i) {
    assert(i < tensor->values->size);
    return tensor->values->values[i];
}

value_t basicGetC(struct Tensor *tensor, struct Vector_S shape) {
    size_t index = 0;

    for(size_t i = 0u; i < shape.size; i++) {
        index += shape.values[i] * tensor->levelSizes.values[i];
    }
    
    return basicGet(tensor, index);
}


//// MISC 


static void printSpaces(size_t count) {
    for(size_t i = 0u; i < count; i++) {
        putchar(' ');
    }
}


static void printTensorPart(struct Tensor *tensor, size_t index, size_t depth) {

    size_t tensorDepthDifference = tensor->shape.size - depth;
 
    if(tensorDepthDifference == 0) {
        printf(VAL_T_FORMAT, tensor->get(tensor, index));   
        return;
    }

    size_t curLevelSize = tensor->levelSizes.values[depth];
    size_t parts = tensor->shape.values[depth];

    if(tensorDepthDifference == 1) {
        printSpaces(depth * 2);
        putchar('[');
        for(size_t i = 0u; i < parts; i++) {
            printf(VAL_T_FORMAT, tensor->get(tensor, index + i));
            if(i < parts - 1) {
                printf(", ");
            }
        }
        printf("]\n");
        return;
    }
    
    size_t partsX = tensor->shape.values[depth + 1];


    if(tensorDepthDifference == 2) { 
        for(size_t y = 0u; y < parts; y++) {
            printSpaces(depth*2);
            putchar(y == 0 ? '[' : ' ');
            for(size_t x = 0u; x < partsX; x++) {
                printf(VAL_T_FORMAT, tensor->get(tensor, index + y * parts + x));
                if(y < parts - 1 || x < partsX - 1) {
                    printf(",\t");
                } else {
                    putchar(']');
                } 
            }
            putchar('\n');
        }
        return;
    }

    printSpaces(depth * 2);
    printf("[\n");
    for(size_t i = 0u; i < parts; i++) {
        printTensorPart(tensor, index + i * curLevelSize, depth + 1);
    }
    printSpaces(depth * 2); 
    printf("]\n");

}

void printTensor(struct Tensor tensor) {
    printTensorPart(&tensor, 0, 0); 
}



////// GENERATOR PRESETS 


value_t zerosGenerator(size_t i) {
    return 0;
}  

value_t onesGenerator(size_t i) { 
    return 1;
}

value_t indexGenerator(size_t i) {
    return i;
}


struct Tensor zeros(struct Vector_S shape) {
    return cTensorByFun(shape, zerosGenerator);
}

struct Tensor ones(struct Vector_S shape) {
    return cTensorByFun(shape, onesGenerator);
}

struct Tensor indexed(struct Vector_S shape) {
    return cTensorByFun(shape, indexGenerator);
}





















