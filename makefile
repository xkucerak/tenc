CFLAGS += -Wall -std=c99

SRC = sandbox.c tensor.c
OBJ = ${SRC:.c=.o} 
DEP = ${SRC:.c=.d}

-include $(DEP)

sandbox: $(OBJ)
	gcc -o $@ $^

clean: 
	rm -f $(DEP) $(OBJ) sandbox
