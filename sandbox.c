#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "tenc.h"


int main() {

    puts("Creating shape");
    size_t shapeValues[3] = {3, 3, 3};
    struct Vector_S shape = createVector(3, shapeValues);

    puts("Creating coords");
    size_t coords[2] = {1, 1};
    struct Vector_S coord = createVector(2, coords);

    puts("Creating tensor");
    struct Tensor tensor = indexed(shape); 
    printTensor(tensor); 

    puts("Creating another tensor");
    struct Tensor other = cTensorByFunLike(&tensor, onesGenerator);
    printTensor(other); 

    printf(VAL_T_FORMAT"\n", tensor.getC(&tensor, coord)); 

    destroyVector(&coord);
    destroyTensor(tensor);
}
