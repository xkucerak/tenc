#include <stdio.h>
#include <stdbool.h>

typedef double value_t;
#define VAL_T_FORMAT "%lf"

////////////////////////////////////////////
/// VECTOR 
////////////////////////////////////////////

struct Vector_S {
    size_t *values;
    size_t size;
};

size_t vectorSum(struct Vector_S vector);

struct Vector_S createVector(size_t size, size_t* shape);

struct Vector_S copy(struct Vector_S vector);

void destroyVector(struct Vector_S *vector);

void printVectorS(struct Vector_S vector);

////////////////////////////////////////////
/// TENSOR
////////////////////////////////////////////

struct ValueContainer {
    value_t* values;
    size_t size;
    size_t references;
    
};

struct Tensor {
    struct ValueContainer *values;
    struct Vector_S shape;
    struct Vector_S levelSizes;
    
    value_t (*get)(struct Tensor*, size_t);    
    value_t (*getC)(struct Tensor*, struct Vector_S shape);
};

////// GETTING /////

value_t basicGet(struct Tensor *tensor, size_t i);

value_t basicGetC(struct Tensor *tensor, struct Vector_S shape);

////// GENERATOR PRESETS //////

struct Tensor cTensorByFun(struct Vector_S shape, value_t (*generator)(size_t));

struct Tensor cTensorByFunLike(struct Tensor *tensor, value_t (*generator)(size_t));

value_t zerosGenerator(size_t i);

value_t onesGenerator(size_t i); 

value_t indexGenerator(size_t i);

struct Tensor zeros(struct Vector_S shape);

struct Tensor ones(struct Vector_S shape);

struct Tensor indexed(struct Vector_S shape);

void destroyTensor(struct Tensor tensor);

void printTensor(struct Tensor tensor);

